
var tui = require('tui-code-snippet');
var util = require('./util');

var ToolTipItem = tui.defineClass({
    /**
     * add tooltip event
     * @param {Object} options - tooltip make options
     */
    init: function(options) {
        this.selectorName = options.selectorName;
        this.contents = options.contents;
        this.delay = options.delay;
        this.targetElements = options.targetElements;
        this.onMouseOverHandler = options.onMouseOverHandler;
        this.onMouseLeaveHandler = options.onMouseLeaveHandler;
        this.timeout = null;
    },
    /**
     * add tooltip event
     */
    addToolTipEvent: function() {
        util.addEventListener(this.targetElements, 'mouseenter', this.onMouseOverHandler);
        util.addEventListener(this.targetElements, 'mouseleave', this.onMouseLeaveHandler);
    },
    /**
     * remove tooltip event
     */
    removeTooltipEvent: function() {
        util.removeEventListener(this.targetElements, 'mouseenter', this.onMouseOverHandler);
        util.removeEventListener(this.targetElements, 'mouseleave', this.onMouseLeaveHandler);
    },
    /**
     * add tooltip element dom
     * @param {HTMLElement} targetElement - tooltip html element
     * @returns {HTMLElement} - wrapped new element
     */
    addToolTipElement: function(targetElement) {
        var tooltipElement = document.createElement('div');

        targetElement = this.wrapingForSelfClosingTag(targetElement);
        tooltipElement.className = 'tooltip';
        tooltipElement.appendChild(document.createTextNode(this.contents));
        targetElement.appendChild(tooltipElement);

        return targetElement;
    },

    /**
     * remove tooltip element dom
     * @param {HTMLElement} targetElement - tooltip html element
     */
    removeToolTipElement: function(targetElement) {
        targetElement.removeChild(targetElement.querySelector('.tooltip'));
    },
    /**
     * wrapping tag for tooltip
     * @param {HTMLElement} targetElement - tooltip html element
     * @returns {HTMLElement}  - remake tooltip html element
     */
    wrapingForSelfClosingTag: function(targetElement) {
        var newTargetElement = null;
        if (targetElement.tagName.toLowerCase() === 'img' && targetElement.className.match(/tooltip-event/)) {
            util.removeClass(targetElement, 'tooltip-event');
            newTargetElement = document.createElement('span');
            targetElement.parentNode.insertBefore(newTargetElement, targetElement);
            newTargetElement.appendChild(targetElement);
            targetElement = newTargetElement;
            util.addClass(newTargetElement, 'tooltip-event');
        }

        return targetElement;
    }

});

module.exports = ToolTipItem;
