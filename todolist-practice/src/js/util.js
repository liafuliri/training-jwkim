
var util = {
    /**
     * add Class
     * @param {HTMLElement} element - target element
     * @param {string} className - class name
     */
    addClass: function(element, className) {
        element.className += ' ' + className;
    },
    /**
     * add Class
     * @param {HTMLElement} element = target element
     * @param {string} className - class name
     */
    removeClass: function(element, className) {
        var origClassName = element.className;
        var re = new RegExp('\\s?' + className + '\\s?', 'g');
        element.className = origClassName.replace(re, '');
    },
    /**
     * find element target
     * @param {EventObject} event - eventObject
     * @returns {HTMLElement} eventTarget = target element
     */
    findEventTarget: function(event) {
        var eventTarget = null;
        event = event || window.event;
        eventTarget = event.target || event.srcElement;

        return eventTarget;
    },
    /**
     * html template function
     * @param {string} template - template target string
     * @param {Object} setting - setting value object
     * @returns {string} template - maked new template
     */
    template: function(template, setting) {
        var matchList = template.match(/{\$([^}]+)}/g);
        if (matchList) {
            util.forEach(matchList, function(replaceTarget) {
                var findSettingName = replaceTarget.replace(/^\{\$|\}/g, '');
                template = template.replace(replaceTarget, setting[findSettingName]);
            });
        }

        return template;
    },
    /**
     * foreach util
     * @param {string} targetList - template target string
     * @param {Function} callback - loop callback
     */
    forEach: function(targetList, callback) {
        var length = targetList.length;
        var i = 0;
        for (; i < length; i += 1) {
            callback(targetList[i], i);
        }
    },
    /**
     * addEventHandler
     * @param {HTMLElement} element - template target string
     * @param {string} type - loop callback
     * @param {Function} handler - handler function
     */
    addEventHandler: function(element, type, handler) {
        if (element.addEventListener) {
            element.addEventListener(type, handler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + type, handler);
        } else {
            element['on' + type] = handler;
        }
    }
};

module.exports = util;
